var Sequelize = require('sequelize');

var attributes = {
  firstName: {
    type: Sequelize.STRING,
  },
  lastName: {
    type: Sequelize.STRING,
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      is: /^[a-z0-9\_\-]+$/i,
    }
  },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true
    }
  },
  phone: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING,
  },
  profile_photo: {
    type: Sequelize.TEXT
  },
  cover_photo: {
    type: Sequelize.TEXT
  },
  salt: {
    type: Sequelize.STRING
  },
  date_joined: {
    type: Sequelize.DATE
  }
}

var options = {
  freezeTableName: true
}

module.exports.attributes = attributes;
module.exports.options = options;
