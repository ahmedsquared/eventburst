var Sequelize = require("sequelize")

var attributes = {
  uid: {
    type:Sequelize.INTEGER,
    allowNull: false,
    validate: {
      is: /^[0-9]+$/i,
    }
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      is: /^[a-z0-9\_\-]+$/i,
    }
  },
  token: {
    type: Sequelize.STRING(3000),
    allowNull: false
  },
  active: {
    type: Sequelize.BOOLEAN,
    allowNull: false
  }
}

var options = {
  freezeTableName: true
};

module.exports.attributes = attributes;
module.exports.options = options;
