var UserMeta = require('./User');
var AuthMeta = require('./AuthMeta')
var connection = require('../sequelize');

const Users = connection.define('users', UserMeta.attributes, UserMeta.options);
const Auth = connection.define('authsession', AuthMeta.attributes, AuthMeta.options);

module.exports = {
  Users: Users,
  Auth: Auth
};
