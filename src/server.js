var restify = require('restify');
var fs = require('fs');
var models = require('./model/models');
var accounts = require('./routes/accounts');
var api = require('./routes/api');
var path = require('path');
var restifyjwt = require('restify-jwt');// used to sign, create, verify tokens.
var jwt = require('jsonwebtoken');
var config = require('./config');
const Users = require("./model/models");
var crypto = require("crypto");


const port = process.env.port || 3000;

// Initialize Restify.
const server = restify.createServer({
  name: "Eventburst"
});

// Log to the server.
server.use((req, res, next) => {
  console.log(req.method + ' ' + req.url);
  return next();
});

// restify body parser.
server.use( restify.bodyParser() );

// Static render
server.get(/static\/?(bundles|images|media)\/.*/, restify.serveStatic({
  directory: __dirname+"/public"
}));

// check token to see if its valid before every request by default.
const verifyToken = function(req,res,next) {
  var token = req.body.token || req.query.token || req.headers['Authorization'];
    if (token) {
    // verifies secret and checks exp
        restifyjwt.verify(token, config.secret, function(err, decoded) {
            if (err) { //failed verification.
                return res.json({"error": true});
            }
            req.decoded = decoded;
            next(); //no error, proceed

            return 0;
        });
    } else {
      console.log("made it");
        // forbidden without token
        return res.status(403).send({
            "error": true
        });
    }
    console.log("made it");
}

// Render Index.html file.
server.get(/.*/, function(req, res, next){

  fs.readFile(__dirname + '/public/index.html', function (err, data) {
    if (err) {
      next( err );
      return;
    }

    if(req.headers.cookie != undefined){
      var authToken = req.headers.cookie.split(";")[0].split("=")[1];
      var cookies = req.headers.cookie.split(";")[0].split("=");
      console.log(cookies);
      for(var i = 0; i < cookies.length; i++){
        if(cookies[i] == "authToken"){
          authToken = cookies[i+1];
          break;
        }
      }
    }

    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Authorization', "Bearer " + authToken );
    res.writeHead(200);
    res.end(data);
    next();
  });
});

// Authentication Routes.
server.post('api/authenticate', accounts.login);
server.post('api/register', accounts.register );
server.post('api/presence', accounts.getToken);
server.post('api/logout', accounts.logout);
server.post('api/user', accounts.getUser);

// Protected Routes
server.post('api/feed', api.feed);

// Start Node Server.
const serverInstance = server.listen(port, () => {
  console.log('App is running on ' + port);
});

// Set up Socket.io server.
const io = require('socket.io').listen(serverInstance);
