// @flow
import React, {Component, Proptypes} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  BrowserRouter,
  Router,
  Route,
  Switch,
  Miss,
  Match } from 'react-router-dom';

import {asyncComponent} from '../utils/asyncComponent'
import Navigation from '../components/Navigation.react'

import * as AppActions from '../actions/AppActions'


require("../base.scss");
require("../components/content.scss");

// Now handle imports.
const Feed = asyncComponent(() => System.import("../components/pages/Feed.react").then(module => module.default));
const Explore = asyncComponent(() => System.import("../components/pages/Explore.react").then(module => module.default));
const Discover = asyncComponent(() => System.import("../components/pages/discover.react").then(module => module.default));
const Messages = asyncComponent(() => System.import("../components/pages/Messages.react").then(module => module.default));
const Contacts = asyncComponent(() => System.import("../components/pages/Contacts.react").then(module => module.default));
const Profile = asyncComponent(() => System.import("../components/pages/Profile.react").then(module => module.default));
const Home = asyncComponent(() => System.import("../components/pages/Home.react").then(module => module.default));
const Search = asyncComponent(() => System.import("../components/pages/Search.react").then(module => module.default));
const NotFound = asyncComponent(() => System.import("../components/pages/NotFound.react").then(module => module.default));

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname:string, cvalue:string, exdays: number ) : void {
    var d = new Date();
    d.setTime(d.getTime() + ( exdays * 24 * 60 * 60 * 1000) );
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function clearCookie(name, domain, path){
    document.cookie = encodeURIComponent(name) + "=deleted; expires=" + new Date(0).toUTCString();
};

class App extends Component {

  constructor( props ){
    super(props);
  }

  componentWillMount(){
    // check to see if user is logged in after refreshing the page.
    this.props.loadUserFromToken();
  }

  componentDidMount(){
    // start finding location.
    this.props.actions.getLocation();
  }

  _routes(){
    var b = console.log;
    var w = console.error;
    console.log = function() {}
    console.error = () => {};
    if( this.props.viewer.isLoggedIn == true ){
      // if someone tries to be slick and plays with react dev tools.
      if( this.props.viewer.id == undefined ){//means no body really there.
        this.props.actions.logoutUser().then(response => {
          let data = response.payload.data;
        });
        console.log = b;//back to norm.
        console.error = w;
      }
      return (
        <Switch>
          <Route exact path="/" render={ (props) =>(<Feed {...props} {...this.props} />) } />
          <Route path="/explore" component={Explore} />
          <Route path="/discover" component={Discover} />
          <Route path="/messages" component={Messages} />
          <Route path="/contacts" component={Contacts} />
          <Route path="/search" render={(props) => (<Search {...props} {...this.props} />)} />
          <Route path="/:username" render={(props) => (<Profile {...props} {...this.props} />)} />
        </Switch>
      );
    } else {
      // no playing with isLoggedIn please.
      if(this.props.viewer.id != undefined){
        this.props.loadUserFromToken();
      }
      return (
        <Switch>
          <Route exact path="/" render={ (props) => (<Home {...props} {...this.props} />) } />
          <Route path="/search" render={(props) => (<Search {...props} {...this.props} />)} />
          <Route path="/:page" render={ (props) =>(<Home {...props} {...this.props} />) } />
          <Route render={ (props) =>(<NotFound {...props} {...this.props} />) } />
        </Switch>
      );
    }
  }

  render(){
    return (
      <BrowserRouter>
        <div id="app">
          <Navigation {...this.props}/>
          <div className="content_container">
            <div className="content_wrap">
              {this._routes()}
            </div>
          </div>
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = state => ({
  viewer: state.viewer,
  user: state.user,
  messages: state.messages,
  events: state.events,
  location: state.location
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(AppActions, dispatch),
  loadUserFromToken: () => {
    var token = localStorage.getItem("EB_USERID") == null?"":localStorage.getItem("EB_USERID");

    dispatch(AppActions.userFromToken(token))
      .then(response => {
        var data = response.payload.data;
        if(data.token == undefined){//is empty
          //flush localStorage key.
          localStorage.removeItem("EB_USERID");
        }
      });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
