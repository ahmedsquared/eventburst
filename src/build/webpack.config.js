var path = require('path');
var webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, './'),
  entry: {
    App: './index.js',
    commons: ['jquery']
  },
  output: {
    path: path.resolve(__dirname, "../public/static"),
    filename: "./bundles/[name].js",
    publicPath: "http://127.0.0.1:3000/static/"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              modules: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react']
          }
        }]
      },
      {
        test: /\.png$/,
        exclude: /node_modules/,
        use: ["url?limit=100000&mimetype=image/png"]
      },
      {
        test: /\.jpg/,
        exclude: /node_modules/,
        use: ['file']
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
        use: ['file?name=public/fonts/[name].[ext]']
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin("./webpack-commons"),
    //new webpack.optimize.UglifyJsPlugin({minimize: true, compress:{ warnings: false }})
  ]
};
