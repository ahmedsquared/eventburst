// @flow
import React, {Component} from 'react';
export const asyncComponent = (getComponent: Function) => class AsyncComponent extends Component {

  state : {
    Component : Object
  }

  constructor(props:Object){
    super(props);

    // const Component = null;
    this.state = {
      Component : AsyncComponent.Component
    }
  }

  componentDidMount(){
    if(!this.state.Component){
      getComponent().then( ( Component:Object ) : void => {
        AsyncComponent.Component = Component
        this.setState({ Component });
      });
    }
  }

  componentWillUnmount(){
    this.setState({Component: {}});
  }

  render(){
    const {Component} = this.state;
    if(Component){
      return <Component {...this.props} />
    }
    return null;
  }
}
