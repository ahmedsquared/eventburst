import {LOGIN_USER_REQUEST, LOGIN_USER_RECIEVED, USER_FROM_TOKEN_RECIEVED, LOGOUT_RECIEVED} from '../constants/ActionTypes';

const initialState = localStorage.getItem("EB_USERID") ? {isLoggedIn: true} : {isLoggedIn: false};

export default function viewer( state = initialState, action ) {

  switch(action.type){
    case LOGIN_USER_RECIEVED:
      if(action.payload.data.success){
        state = action.payload.data.viewer;
      }
      return state;
      break;
    case USER_FROM_TOKEN_RECIEVED:
      if(action.payload.data.success){
        state = action.payload.data.viewer;
      } else {
        state = {isLoggedIn: false};
      }
      return state;
      break;
    case LOGOUT_RECIEVED:
      if(action.payload.data.success){
        state = {isLoggedIn: false};
      }
      return state;
      break;
    default:
      return state;
      break;
  }

}
