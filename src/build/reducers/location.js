import {LOCATION_RECIEVED, LOCATION_REQUEST} from '../constants/ActionTypes';

const initialState = {
  latitude: 25.2048,
  longitude: 55.2708
};

export default function location(state = initialState, action){
  switch (action.type) {
    case LOCATION_RECIEVED:
      return {
        longitude: action.payload.longitude,
        latitude: action.payload.latitude,
        accuracy: action.payload.accuracy
      };
      break;
    default:
      return state;
      break;
  }
}
