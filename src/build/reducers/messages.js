import { ADD_MESSAGE, GET_MESSAGES } from '../constants/ActionTypes'

const initialState = [];

export default function messages( state = initialState, action ) {
  if(action.type == ADD_MESSAGE){
    return [
      ...state,
      {
        id: state.reduce( (maxId, message) => Math.max( message.id, maxId ), -1 ) + 1,
        username: action.payload.username,
        message: action.payload.message
      }
    ];
  } else {
    return state;
  }

  switch( action.type ){
  }

}
