import {combineReducers} from "redux";
import { routerReducer as routing } from 'react-router-redux';
import messages from './messages';
import viewer from './viewer';
import user from './user';
import location from './location';

const rootReducer = combineReducers({
  messages,
  viewer,
  user,
  location
});

export default rootReducer;
