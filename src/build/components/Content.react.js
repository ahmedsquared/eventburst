import React, {Component, PropTypes} from 'react'

require("./content.scss");

export default class Content extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <div className="content_container">
        <div className="content_wrap">
          {this.props.children}
        </div>
      </div>
    )
  }
}
