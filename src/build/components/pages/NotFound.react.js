import React, {Component, PropTypes} from 'react'
import Navigation from '../Navigation.react'
import SideNav from '../SideNav.react'
import Content from '../Content.react'
import {
  Route,
  Switch, Link } from 'react-router-dom';

export default class NotFound extends Component {

  constructor(props){
    super(props);
  }

  componentWillMount(){
    var elem = document.querySelector("#app");
    elem.className = "";
  }

  render(){
    return (
      <div className="nothing_container">
        Nothing Here get going.
      </div>
    );
  }
}
