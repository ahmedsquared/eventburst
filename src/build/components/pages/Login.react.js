import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import Paper from '../Paper.react'

require("./login.scss");

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export default class Login extends Component {

  constructor(props){
    super(props);

    this.state = {
      username: "",
      password: "",
      hasError: false,
      error: ""
    }
  }

  componentWillMount(){
    var root = document.querySelector("#app");
    root.className = "waterfall";
  }

  _handleLogin(evt){
    evt.preventDefault();
    const {actions} = this.props;

    actions.loginUser(this.state.username.toLowerCase(), this.state.password)
                      .then(response => {
                        let data = response.payload.data;
                        // all is good.
                        if(data.success == true){
                          // set sessionStorage as well.
                          localStorage.setItem("EB_USERID", data.token);
                        } else {
                          // show error message.
                          this.setState({hasError:true, error: data.message});
                        }
                      });

  }

  _showError(){
    return (
      <div className="error-box">
        <span>{this.state.error}</span>
      </div>
    );
  }

  render(){
    return (
      <div className="bottom-area">
        <div className="front-content">
          <div className="auth-area">
            <Paper style="login_container">
              <div className="title">
                <h1>Log In</h1>
              </div>
              <div className="login_form">
                {this.state.hasError?this._showError():null}
                <form className="form" onSubmit={this._handleLogin.bind(this)}>
                  <input type="text" className="inputtext" name="username" id="username" onChange={(evt) => this.setState({username:evt.target.value})} placeholder="Username or Phone Number" />
                  <input type="password" className="inputtext" name="password" id="password" onChange={(evt) => this.setState({password:evt.target.value})} placeholder="Password" />
                  <div className="forgot">
                    <a href="/accounts/forgot" className="text">Forgot your password?</a>
                  </div>
                  <button className="button blue">Log In</button>
                </form>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    )
  }
}
