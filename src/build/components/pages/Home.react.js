import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import Navigation from '../Navigation.react'
import SideNav from '../SideNav.react'
import Content from '../Content.react'
import {
  Route,
  Switch } from 'react-router-dom';

import {asyncComponent} from '../../utils/asyncComponent'

require("./home.scss");

const Signup = asyncComponent(() => System.import("./signup.react").then(module => module.default));
const Login = asyncComponent(() => System.import("./Login.react").then(module => module.default));
const NotFound = asyncComponent(() => System.import("./NotFound.react").then(module => module.default));

export default class Home extends Component {

  constructor(props){
    super(props);
  }

  componentWillMount(){
    var root = document.querySelector("#app");
    root.className = "waterfall";
  }

  render(){
    return (
      <div className="front">
        <Switch>
          <Route exact path="/" render={(props) => (<Login {...this.props}/>)} />
          <Route path="/signup" render={(props) => (<Signup {...this.props}/>)} />
          <Route path="/login" render={(props) => (<Login {...this.props}/>)} />
          <Route path="/:username" render={(props) => (<NotFound {...this.props}/>)} />
        </Switch>
      </div>
    )
  }
}
