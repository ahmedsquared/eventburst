import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router-dom'
import Map from '../mapbox'

function clearCookie(name, domain, path){
    document.cookie = encodeURIComponent(name) + "=deleted; expires=" + new Date(0).toUTCString();
};
require("./feed.scss");
export default class Feed extends Component {

    constructor(props){
        super(props);
    }

    componentWillMount(){
        var root = document.querySelector("#app");
        root.className = "blueBar";
    }

  render(){
    return (
        <div className="feed_container">
            <Map elemId="feedmap" long={this.props.location.longitude} lat={this.props.location.latitude} zoom={15}/>
            <div className="mainCol">
              
            </div>
        </div>
    );
  }
}
