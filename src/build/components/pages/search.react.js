import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import Navigation from '../Navigation.react'
import SideNav from '../SideNav.react'
import Content from '../Content.react'
import Paper from '../Paper.react'

export default class Search extends Component {

  constructor(props){
    super(props);
  }

  getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  componentDidMount(){
    var root = document.querySelector("#app");
    root.className="redBar";
  }

  render(){
    const query = this.getParameterByName("q");
    return (
      <div>
        <h1>{query}</h1>
      </div>
    )
  }
}
