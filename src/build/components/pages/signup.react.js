import React, {Component, PropTypes} from 'react'
import Navigation from '../Navigation.react'
import Content from '../Content.react'
import {Route, Link} from 'react-router-dom';
import {registerPostRequest} from '../../actions/AppActions'
import Paper from '../Paper.react'

require("./signup.scss");

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export default class Signup extends Component {

  constructor(props){
    super(props);

    this.state = {
      "name": "",
      "username":"",
      "email":"",
      "password":"",
      hasError: false,
      error: ""
    };
  }

  _updateName(evt){
    this.setState({"name":evt.target.value});
  }

  _updateUsername(evt){
    this.setState({"username":evt.target.value});
  }

  _updateEmail(evt){
    this.setState({"email":evt.target.value});
  }
  _updatePassword(evt){
    this.setState({"password":evt.target.value});
  }


  _checkFields(){
    const {
      name,
      username,
      email,
      password
    } = this.state;

    if( name.trim() == ""){
      return false;
    } else if(username.trim() == ""){
      return false;
    } else if(email.trim() == ""){
      return false;
    } else if(password.trim() == ""){
      return false;
    }

    return true;
  }

  _createAccount(evt){
    evt.preventDefault();
    var form = evt.target;

    //submit form data collected in state.
    if( this._checkFields() ){
      this.props.actions.registerUser( this.state.name, this.state.username,
                                       this.state.email, this.state.password )
                        .then(response => {
                          let data = response.payload.data;

                          // everything is good.
                          if(data.success){
                            this.setState({hasError: false});
                            this.props.actions.loginUser(this.state.username.toLowerCase(), this.state.password)
                                              .then(response => {
                                                let data = response.payload.data;
                                                // all is good.
                                                if(data.success == true){
                                                  this.setState({hasError:false});
                                                  localStorage.setItem("EB_USERID", data.token);
                                                }
                                              });
                          } else {
                            this.setState({hasError: true, error: data.message});
                          }
                        });
    } else {
      alert("Please enter in all values.");
    }
  }

  componentWillMount(){
    var root = document.querySelector("#app");
    root.className = "waterfall";
  }

  render(){
    return (
      <div className="bottom-area">
        <div className="front-content">
          <div className="auth-area">
            <Paper style="signup-container">
              <div className="title">
                <h1>Join Eventburst</h1>
              </div>
              <div className="signup-content">
                <form id="signup-form" onSubmit={this._createAccount.bind(this)}>
                  <div className="signup-input-fields">
                    <input type="text" onChange={this._updateName.bind(this)} className="inputtext" name="full_name" id="full_name" placeholder="Full Name" />
                    <input type="text" onChange={this._updateUsername.bind(this)} className="inputtext" name="username" id="username" placeholder="Username" />
                    <input type="text" onChange={this._updateEmail.bind(this)} className="inputtext" name="email" id="email" placeholder="Email or Mobile Number"/>
                    <input type="password" onChange={this._updatePassword.bind(this)} className="inputtext" name="password" id="password" placeholder="Password"/>
                  </div>
                  <div className="signup-form-actions">
                    <button className="button blue">Create Account</button>
                    <span className="form-message">
                      By signing up, you agree to our <Link to="/">Terms</Link> & <Link to="/">Privacy Policy</Link>.
                    </span>
                  </div>
                </form>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    )
  }
}
