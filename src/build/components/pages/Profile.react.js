import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router-dom'
import {asyncComponent} from '../../utils/asyncComponent'

export default class Profile extends Component {

  constructor(props){
    super(props);
  }

  // load user data.
  componentWillMount(){
    this._getUsernameData();
  }

  _getUsernameData(){
    // auto set state.
    this.props.actions.getProfileUser(this.props.match.params.username, sessionStorage.getItem("authToken"))
  }

  _renderProfile(){
    return (
      <div>
        <h1>{this.props.user.firstName+" "+this.props.user.lastName}</h1>
      </div>
    );
  }

  _renderPage(){
    const {username} = this.props.match.params;
    if(this.props.viewer.id == undefined){
      if(username == "signup" || username == "login"){
        const Home = asyncComponent(() => System.import("../../components/pages/Home.react").then(module => module.default));
        return <Home {...this.props}/>;
      }
    } else {
      if(username == "signup" || username == "login"){
        this.props.history.replace("/");
      }
      return this._renderProfile();
    }
    return this._renderProfile();
  }

  render(){
    const {username} = this.props.match.params;
    return (
      <div>
        {this._renderPage()}
      </div>
    )
  }
}
