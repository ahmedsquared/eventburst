import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router-dom'

require("./messages.scss");
export default class Messages extends Component {

  constructor(props){
    super(props);

    this.state = {
      currentThread: {
        id: null,
        users:[]
      }
    }
  }

  componentWillMount(){
    var root = document.querySelector("#app");
    root.className = "greenBar waterfall";
  }

  _create(){

  }

  render(){
    const {threadid} = this.props.match.params;
    return (
      <div className="messages-holder">
        <div className="messages-box">
          <div className="left-col">
            <div className="thread-header clearfix">
              <h2>Chats</h2>
              <span className="create-new"><Link to="/messages/new" onClick={this._create.bind(this)}>Create</Link></span>
            </div>
          </div>
          <div className="right-col">
            <div className="message-header">
              <div className="mh-wrap">
                <div className="user-title">Ahmed Ahmed</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
