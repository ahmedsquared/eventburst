import React, {Component} from 'react';
import {Link, Route} from 'react-router-dom'

export default ({label, to, activeOnlyWhenExact}) => (
  <Route path={to} exact={activeOnlyWhenExact} children={({match}) => (
    <div className={match ? 'nav-container active':'nav-container'}><Link to={to}>{label}</Link></div>
  )}/>
)
