import React, {Component, } from 'react';
import Router, { Route, Link, browserHistory} from 'react-router-dom';
import {render} from 'react-dom';
import {asyncComponent} from '../utils/asyncComponent';

require('../colors.scss');
require('./navigation.scss');


const Menu = ({label, to, activeOnlyWhenExact, className}) => (
  <Route path={to} exact={activeOnlyWhenExact} children={({match}) => (
    <div className={match ? 'nav-container active':'nav-container'}><Link to={to} className={className}>{label}</Link></div>
  )}/>
)

export default class Navigation extends Component {

  constructor(props){
    super(props);

    this.state = {
      drawer: false,
      moreMenu: false
    }
  }

  componentDidMount(){
    var url = window.location.pathname;
    var searchInput = document.querySelector(".searchInput");

    document.body.onclick = (evt) => {
      //hide menu if clicked outside.
      if( evt.target.nodeName != "svg"  && evt.target.nodeName != "A"){
        this.setState({moreMenu: false});
      }
    }
  }

  _settingsMenu(evt){
    evt.preventDefault();

    // show menu.
    if( this.state.moreMenu == true ){
      this.setState({moreMenu: false});
    } else {
      this.setState({moreMenu: true});
    }
  }
  _settingsToggle(evt){

    // show menu.
    if( this.state.moreMenu == true ){
      this.setState({moreMenu: false});
    } else {
      this.setState({moreMenu: true});
    }
  }

  _handleLogout(evt){
    evt.preventDefault();

    //clear session.
    if(sessionStorage.getItem("authToken")){
      sessionStorage.removeItem("authToken");
    }
    this.props.actions.logoutUser().then(response => {
      let data = response.payload.data;
      localStorage.removeItem("EB_USERID");
      this.props.history.replace("/");
    });

    //hide menu.
    this.setState({moreMenu: false});
  }

  createEvent(evt){
    evt.preventDefault();

    // Create Event.

  }

  loadMenu(){
    if(this.props.viewer.id != undefined){
      return (
        <div className="nav-items nav-right">
          <ul className="nav-usermenu">
            <li className="text"><Link to="/events/create" onClick={this.createEvent.bind(this)}>Create Event</Link></li>
            <li className="name"><Link to={"/"+this.props.viewer.username}>{this.props.viewer.firstName}</Link></li>
            <li className="menu-more">
              <Link to="/" className="menu more" onClick={this._settingsMenu.bind(this)}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="#000000" height="48" viewBox="0 0 24 24" width="48"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 8c1.1 0 2-0.9 2-2s-0.9-2-2-2-2 0.9-2 2 0.9 2 2 2zm0 2c-1.1 0-2 0.9-2 2s0.9 2 2 2 2-0.9 2-2-0.9-2-2-2zm0 6c-1.1 0-2 0.9-2 2s0.9 2 2 2 2-0.9 2-2-0.9-2-2-2z"/></svg>
              </Link>
              <ul className={this.state.moreMenu?"menu-account-list active":"menu-account-list"}>
                <li>
                  <Link to={"/"+this.props.viewer.username} onClick={this._settingsToggle.bind(this)}>Profile</Link>
                </li>
                <li>
                  <Link to="/discover" onClick={this._settingsToggle.bind(this)}>Discover</Link>
                </li>
                <li>
                  <Link to="/messages" onClick={this._settingsToggle.bind(this)}>Messages</Link>
                </li>
                <li>
                  <Link to="/settings" onClick={this._settingsToggle.bind(this)}>Settings</Link>
                </li>
                <li>
                  <Link to="/logout" onClick={this._handleLogout.bind(this)}>Logout</Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      );
    } else {
      return (
        <div className="nav-items nav-right">
          <Menu to="/login" label="Login" activeOnlyWhenExact={true}/>
          <Menu to="/signup" label="Register" activeOnlyWhenExact={true} className="reg"/>
        </div>
      );
    }
  }

  _handleSearch(e){
    e.preventDefault();
    var history = this._reactInternalInstance._context.router.history;
    if(document.querySelector(".searchInput").value.trim() != ""){
      history.push("/search?q="+document.querySelector(".searchInput").value);
    }
  }

  checkEmpty(evt) {
    var elem = evt.target;
    if(elem.value != "") {
      elem.className = elem.className+" dt";
    } else {
      elem.className = "searchInput";
    }
  }

  render(){
    return (
      <span>
        <header className="header">
          <div className="brand_container">
            <div className="logo_holder">
              <Link to="/" className="logo"></Link>
            </div>
            <div className="search_container" role="search">
              <div className="search_holder">
                <form id="search_form" action="/search" className="search-form" onSubmit={this._handleSearch.bind(this)}>
                  <input type="text" placeholder="Search Places, People and Events" onBlur={this.checkEmpty.bind(this)} name="q" className="searchInput"/>
                </form>
              </div>
            </div>
          </div>
          <div className="nav_container" role="navigation">
            {this.loadMenu()}
          </div>
        </header>
      </span>
    );
  }
}



// <div className="nav-top-wrapper">
//   <div className="nav-top">
//     <div className="nav-items nav-search">
//       <div className="search-box">
//         <form id="search_form" action="/search" className="search-form" onSubmit={this._handleSearch.bind(this)}>
//           <input type="text" placeholder="Search" name="q" className="searchInput"/>
//         </form>
//       </div>
//     </div>
//   </div>
// </div>


// Preserved


// <div className="nav-container"><Link to="/explore" title="Explore">Explore</Link></div>
// <div className="nav-container"><Link to="/discover" title="discover">Discover</Link></div>
// <div className="nav-container"><Link to="/messages" title="Start Chatting">Messages</Link></div>

// <li className="nav-items nav-left">
//   <div className={this.props.location.pathname == "/explore"? "nav-container active":"nav-container"}><Link to="/explore" title="Explore">Explore</Link></div>
//   <div className={this.props.location.pathname == "/events/discover"? "nav-container active":"nav-container"}><Link to="/events/discover" title="discover">Discover</Link></div>
//   <div className={this.props.location.pathname == "/messages"? "nav-container active":"nav-container"}><Link to="/messages" title="Start Chatting">Messages</Link></div>
// </li>
// <li className="nav-items nav-right">
//   <div className={this.props.location.pathname.split("?")[0] == "/search"? "nav-container active":"nav-container"}>
//     <a href="/search" onClick={this._showSearch.bind(this)} title="Search" id="searchLink">Search</a>
//   </div>
//   <div className={this.props.location.pathname == "/login"? "nav-container active":"nav-container"}>
//     <Link to="/login" title="Sign In">Log In</Link>
//   </div>
//   <div className={this.props.location.pathname == "/signup"? "nav-container active":"nav-container"}>
//     <Link to="/signup" title="Sign Up">Sign up</Link>
//   </div>
// </li>
