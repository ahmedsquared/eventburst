import React, {Component} from 'react'

import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken = 'pk.eyJ1IjoiYWhtZWRhYmRpaGFraW0iLCJhIjoiY2oyeXo5MHU3MDAyOTMybnZjaXVqODc2YSJ9.M4L7iSf0JPtFVEsjtogUiQ';

require("./mapbox.scss");
export default class Map extends Component {
  constructor(props){
    super(props);

    this.state = {
      map: {},
      marker: null,
      geojson: this.props.geojson
    };

    this.marker = null;

    this.currentjson = {};
  }

  componentDidMount(){
    var m = new mapboxgl.Map({
      container: this.props.elemId,
      style: 'mapbox://styles/mapbox/dark-v9',
      zoom: this.props.zoom
    });

    // load map inside container.
    m.addControl(new mapboxgl.GeolocateControl());
    this.setState({map : m });
  }

  componentDidUpdate(){
    // clear current location.
    if( this.marker != null ){
      this.clearCurrentLocation();
    }

    this.currentjson = {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "properties": {
            "message": "You are here",
            "iconSize": [10, 10]
          },
          "geometry": {
            "type": "Point",
            "coordinates": [
              this.props.long,
              this.props.lat
            ]
          }
        }
      ]
    };

    // add markers to map
    this.currentjson.features.forEach(function(marker) {
        // create a DOM element for the marker
        var el = document.createElement('div');
        el.className = 'loc_marker';

        el.addEventListener('click', function() {
            window.alert(marker.properties.message);
        });

        // add marker to map
        this.marker = new mapboxgl.Marker(el, {offset: [-marker.properties.iconSize[0] / 2, -marker.properties.iconSize[1] / 2]})
            .setLngLat(marker.geometry.coordinates)
            .addTo(this.state.map);

    }.bind(this));

    //render event markers.
    if( this.state.geojson != undefined){
      this.renderEventMarkers();
    }

    this.state.map.setCenter([this.props.long, this.props.lat]);
  }

  renderEventMarkers(){
    // add markers to map
    this.state.geojson.features.forEach(function(marker) {
        // create a DOM element for the marker
        var el = document.createElement('div');
        el.className = 'event_marker';

        el.addEventListener('click', function() {
            window.alert(marker.properties.message);
        });

        // add marker to map
        this.marker = new mapboxgl.Marker(el, {offset: [-marker.properties.iconSize[0] / 2, -marker.properties.iconSize[1] / 2]})
            .setLngLat(marker.geometry.coordinates)
            .addTo(this.state.map);

    }.bind(this));
  }

  clearCurrentLocation(){
    this.marker = this.marker.remove();
  }

  render(){
    return (
        <div id={this.props.elemId}></div>
    );
  }
}
