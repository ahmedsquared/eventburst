import React, {Component, PropTypes} from 'react'

require("./paper.scss");

export default class Paper extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <div className={"paper "+this.props.style}>
        <div className="paper_wrap">
          {this.props.children}
        </div>
      </div>
    )
  }
}
