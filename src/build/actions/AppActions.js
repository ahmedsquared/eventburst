import * as types from '../constants/ActionTypes'

export const loginPostRequest = (username, password) => ({type: types.LOGIN_USER_REQUEST, payload: {username, password} });
export const loginPostRecieved = (json) => ({type: types.LOGIN_USER_RECIEVED, payload: {data:json} });

export const logoutRequest = (token) => ({type: types.LOGOUT_REQUEST, payload:{token}});
export const logoutReceived = (json) => ({type: types.LOGOUT_RECIEVED, payload:{data:json}});

export const registerPostRequest = ( name, username, email, password ) => ({type: types.REGISTER_USER_REQUEST, payload: {name, username, email, password}});
export const registerPostRecieved = (json) => ({type: types.REGISTER_USER_RECIEVED, payload: {data: json}});

export const userFromTokenRequest = (token) => ({type: types.USER_FROM_TOKEN_REQUEST, payload: {token}});
export const userFromTokenRecieved = (json) => ({type: types.USER_FROM_TOKEN_RECIEVED, payload: {data: json}});

// get user from profile page
export const profileUserRequest = (username) => ({type: types.PROFILE_USER_REQUEST, payload: {username}});
export const profileUserReceived = (json) => ({type: types.PROFILE_USER_RECIEVED, payload: {data:json}});

export const locationRequest = () => ({type: types.LOCATION_REQUEST, payload: {fetching: true} });
export const locationRecieved = (json) => ({type: types.LOCATION_RECIEVED, payload: json });

const getDomain = () => {
  var domain = window.location.hostname;
  var port = window.location.port;

  return domain + (port != "")? domain+":"+port: domain;
}

// Async Actions.
export const registerUser = ( name, username, email, password ) => (dispatch) => {
  dispatch( registerPostRequest(name, username, email, password) );
  return fetch("http://"+getDomain()+"/api/register",{
        headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Origin': ''
              },
        method:"POST",
        body:JSON.stringify( {name, username, email, password} )
      }).then(response => response.json())
        .then( ( json ) => dispatch( registerPostRecieved(json)));
}

export const loginUser = (username, password) => (dispatch) => {
  dispatch( loginPostRequest(username, password) );

  return fetch("http://"+getDomain()+"/api/authenticate", {
    headers: {
      'Accept' : 'application/json',
      'Content-Type' : 'application/json',
      'Origin': ''
    },
    method: "POST",
    body: JSON.stringify({username, password})
  }).then(response => response.json())
    .then( (json) => dispatch(loginPostRecieved(json)) );
}

export const logoutUser = (token) => (dispatch) =>{
  dispatch(logoutRequest(token));
  return fetch("http://"+getDomain()+"/api/logout", {
    headers: {
      'Accept' : 'application/json',
      'Content-Type' : 'application/json',
      'Origin': '',
      'Authorization': token
    },
    method: "POST",
    body: JSON.stringify({token})
  }).then(response => response.json())
    .then( (json) => dispatch(logoutReceived(json)) );
}

export const userFromToken = (token) => (dispatch) => {
  dispatch( userFromTokenRequest(token) );

  return fetch("http://"+getDomain()+"/api/presence", {
    headers: {
      'Accept' : 'application/json',
      'Content-Type' : 'application/json',
      'Origin': '',
      'Authorization': token
    },
    method: "POST",
    body: JSON.stringify({token})
  }).then(response => response.json())
    .then( json => dispatch( userFromTokenRecieved(json) ) );
}

export const getProfileUser = (username, token) => (dispatch) => {
  dispatch(profileUserRequest(username));

  return fetch("http://"+getDomain()+"/api/user", {
    headers : {
      'Accept' : 'application/json',
      'Content-Type' : 'application/json',
      'Origin' : '',
      'Authorization' : token
    },
    method : "POST",
    body : JSON.stringify({username})
  }).then( response => response.json() )
    .then( json => dispatch(profileUserReceived(json)) )
}

/**
 * Get location Data.
 * @return json.
 */
 export const getLocation = () => (dispatch) => {
   dispatch(locationRequest());
   var longitude = null,
       latitude = null,
       json = {};

   // do the fetching of location here. By Default it's set to dubai :).
   if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(getPosition);
    } else {
      console.log(json);
    }
    function getPosition(position){
      var json = {
        longitude:position.coords.longitude,
        latitude:position.coords.latitude,
        accuracy: position.coords.accuracy
      }
      dispatch(locationRecieved(json));
    }
 }
