var AWS = require('aws-sdk');

AWS.config.loadFromPath("./aws.config.json");

export const s3 = new AWS.S3({params : {Bucket: "eventburst-media"}});
