const Users = require("../model/models").Users;
const Auth = require("../model/models").Auth;

var jwt = require("jsonwebtoken")
var crypto = require("crypto");
var config = require("../config");

function AccountController(){
  var _this = this;

  // Login Script.
  _this.login = ( req, res, next ) => {
    var data = {
      "Messages": "Login Request empty."
    }
    var password = req.params.password;
    var shasum = crypto.createHash('sha1');

    // Find the user.
    Users.findOne({
      where:{username: req.params.username}
    }).then(user => {

      if(!user){
        res.json({success: false, message:'Login Failed. Try Again. Hint: It might be your Username.'});
      } else if(user){
        shasum.update( password + "" + user.salt );
        var passwordHash = shasum.digest('hex');
        if(user.password != passwordHash){
          res.json({success: false, message:'Sorry your password was incorrect. Please enter the correct password to continue.'});
        } else {
          // Everything is good here now lets create the token.
          var token = jwt.sign({user:user, exp: 1382412921}, config.secret);

          var auth = Auth.build({uid:user.id, username: user.username, token: token, active: true});
          auth.save();

          user = user.toJSON();
          // remove password from user object.
          delete user.password;
          delete user.salt;
          user.isLoggedIn = true;

          // return the token.
          res.json({
            success: true,
            message: "Success",
            token: token,
            viewer: user
          });
        }
      }

    });

    return next();
  }

  _this.register = ( req, res, next ) => {
    var data = {};

    //salting something here. Salt Bae.
    var salty = crypto.randomBytes(16);

    // handle data and clean it. It may be dirty.
    var name = req.params.name,
        names = name.split(" "),
        firstName = "",
        lastName = "",
        username = req.params.username.toLowerCase(),
        email = req.params.email.toLowerCase(),
        password = req.params.password,
        salt = salty.toString('base64');

    // hash password.
    var shasum = crypto.createHash('sha1');
    shasum.update(password+salt);
    var passwordHash = shasum.digest('hex');

    //build names middle names will be ignored.
    if(names.length > 1){
      firstName = names[0];
      lastName = names[names.length-1];
    } else {
      firstName = names[0];
    }

    // check if username is taken.
    Users.findOne({
      where: {username: username},
      attributes: {excludes:['password', 'salt']}
    }).then(user => {
      if(user){
        res.json({
          "success": false,
          "message": "Username is taken"
        });
      }
    });

    // Check if email is taken,
    Users.findOne({
      where : {email: email},
      attributes: {excludes:['password', 'salt']}
    }).then(user => {
      if(user){
        res.json({
          "success": false,
          "message": "Email is taken"
        });
      }
    });

    var users = Users.build({firstName, lastName, username, email, password:passwordHash, salt});

    if( req.params.password.length != "" ){
      users.save().then(user => {
        res.json({
          "success":true,
          "message": "Registration Successful",
          "data":user
        });
      }).catch(function(error){
        res.json({
          "success":false,
          "message":"Registration Failure",
          "error": error
        });
      });
    }

    return next();
  }

  _this.getUser = (req, res, next) => {
    Users.findOne({
      where : {username:req.params.username},
      attributes: {exclude:['password', 'salt']}
    }).then( user => {
      if(user) {
        res.json(user);
      }
    });
  }

  _this.getToken = (req, res, next) => {

    // check to see if token is present in header or request url.
    var token = "";
    if(req.headers.authorization){
      token = req.headers.authorization
    } else if(req.params.authToken){
      token = req.params.authToken;
    }


    // Look for the token and see if its valid.
    Auth.findOne({
      where : {
        token : token
      }
    }).then(auth => {
      if(!auth){
        res.json({
          success: false,
          message: "Token not present."
        });
      } else if(auth){
        Users.findOne({
          where: {username: auth.username},
          attributes: { exclude: ['password', 'salt'] }
        }).then(user => {
          if(user){
            user = user.toJSON();
            user.isLoggedIn = true;
            res.json({
              success: true,
              message: "Token is there.",
              token: auth.token,
              viewer: user
            });
          }
        });
      }
    });
  }

  _this.logout = (req, res, next) => {
    var token = req.params.token;

    Auth.findOne({
      where : {
        token : token,
        active : true
      }
    }).then(auth => {
      if( auth ){
        auth.updateAttributes({
          active: false
        });
        auth.save();
        res.json({
          success: false,
          message: "User session is now set to logout."
        });
      } else if( !auth ){
        res.json({
          success: true,
          message: "User session does not exists or already deleted."
        });
      }
    });
  }

}

module.exports = new AccountController();
