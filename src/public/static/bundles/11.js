webpackJsonp([11],{

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

__webpack_require__(381);

var Paper = function (_Component) {
  _inherits(Paper, _Component);

  function Paper(props) {
    _classCallCheck(this, Paper);

    return _possibleConstructorReturn(this, (Paper.__proto__ || Object.getPrototypeOf(Paper)).call(this, props));
  }

  _createClass(Paper, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "div",
        { className: "paper " + this.props.style },
        _react2.default.createElement(
          "div",
          { className: "paper_wrap" },
          this.props.children
        )
      );
    }
  }]);

  return Paper;
}(_react.Component);

exports.default = Paper;

/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)();
// imports


// module
exports.push([module.i, ".paper {\n  margin: 0 auto;\n  background-color: #FFF;\n  border-radius: 2px;\n  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.15);\n  max-width: 980px; }\n\n.paper_wrap {\n  margin: 10px; }\n", ""]);

// exports


/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(379);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(40)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./paper.scss", function() {
			var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./paper.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _Navigation = __webpack_require__(160);

var _Navigation2 = _interopRequireDefault(_Navigation);

var _Content = __webpack_require__(376);

var _Content2 = _interopRequireDefault(_Content);

var _reactRouterDom = __webpack_require__(88);

var _AppActions = __webpack_require__(168);

var _Paper = __webpack_require__(378);

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

__webpack_require__(398);

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var Signup = function (_Component) {
  _inherits(Signup, _Component);

  function Signup(props) {
    _classCallCheck(this, Signup);

    var _this = _possibleConstructorReturn(this, (Signup.__proto__ || Object.getPrototypeOf(Signup)).call(this, props));

    _this.state = {
      "name": "",
      "username": "",
      "email": "",
      "password": "",
      hasError: false,
      error: ""
    };
    return _this;
  }

  _createClass(Signup, [{
    key: '_updateName',
    value: function _updateName(evt) {
      this.setState({ "name": evt.target.value });
    }
  }, {
    key: '_updateUsername',
    value: function _updateUsername(evt) {
      this.setState({ "username": evt.target.value });
    }
  }, {
    key: '_updateEmail',
    value: function _updateEmail(evt) {
      this.setState({ "email": evt.target.value });
    }
  }, {
    key: '_updatePassword',
    value: function _updatePassword(evt) {
      this.setState({ "password": evt.target.value });
    }
  }, {
    key: '_checkFields',
    value: function _checkFields() {
      var _state = this.state,
          name = _state.name,
          username = _state.username,
          email = _state.email,
          password = _state.password;


      if (name.trim() == "") {
        return false;
      } else if (username.trim() == "") {
        return false;
      } else if (email.trim() == "") {
        return false;
      } else if (password.trim() == "") {
        return false;
      }

      return true;
    }
  }, {
    key: '_createAccount',
    value: function _createAccount(evt) {
      var _this2 = this;

      evt.preventDefault();
      var form = evt.target;

      //submit form data collected in state.
      if (this._checkFields()) {
        this.props.actions.registerUser(this.state.name, this.state.username, this.state.email, this.state.password).then(function (response) {
          var data = response.payload.data;

          // everything is good.
          if (data.success) {
            _this2.setState({ hasError: false });
            _this2.props.actions.loginUser(_this2.state.username.toLowerCase(), _this2.state.password).then(function (response) {
              var data = response.payload.data;
              // all is good.
              if (data.success == true) {
                _this2.setState({ hasError: false });
                localStorage.setItem("EB_USERID", data.token);
              }
            });
          } else {
            _this2.setState({ hasError: true, error: data.message });
          }
        });
      } else {
        alert("Please enter in all values.");
      }
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      var root = document.querySelector("#app");
      root.className = "waterfall";
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'bottom-area' },
        _react2.default.createElement(
          'div',
          { className: 'front-content' },
          _react2.default.createElement(
            'div',
            { className: 'auth-area' },
            _react2.default.createElement(
              _Paper2.default,
              { style: 'signup-container' },
              _react2.default.createElement(
                'div',
                { className: 'title' },
                _react2.default.createElement(
                  'h1',
                  null,
                  'Join Eventburst'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'signup-content' },
                _react2.default.createElement(
                  'form',
                  { id: 'signup-form', onSubmit: this._createAccount.bind(this) },
                  _react2.default.createElement(
                    'div',
                    { className: 'signup-input-fields' },
                    _react2.default.createElement('input', { type: 'text', onChange: this._updateName.bind(this), className: 'inputtext', name: 'full_name', id: 'full_name', placeholder: 'Full Name' }),
                    _react2.default.createElement('input', { type: 'text', onChange: this._updateUsername.bind(this), className: 'inputtext', name: 'username', id: 'username', placeholder: 'Username' }),
                    _react2.default.createElement('input', { type: 'text', onChange: this._updateEmail.bind(this), className: 'inputtext', name: 'email', id: 'email', placeholder: 'Email or Mobile Number' }),
                    _react2.default.createElement('input', { type: 'password', onChange: this._updatePassword.bind(this), className: 'inputtext', name: 'password', id: 'password', placeholder: 'Password' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'signup-form-actions' },
                    _react2.default.createElement(
                      'button',
                      { className: 'button blue' },
                      'Create Account'
                    ),
                    _react2.default.createElement(
                      'span',
                      { className: 'form-message' },
                      'By signing up, you agree to our ',
                      _react2.default.createElement(
                        _reactRouterDom.Link,
                        { to: '/' },
                        'Terms'
                      ),
                      ' & ',
                      _react2.default.createElement(
                        _reactRouterDom.Link,
                        { to: '/' },
                        'Privacy Policy'
                      ),
                      '.'
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Signup;
}(_react.Component);

exports.default = Signup;

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)();
// imports


// module
exports.push([module.i, ".signup-container {\n  background: #FFF;\n  border-radius: 3px;\n  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);\n  display: block;\n  margin: 0 auto;\n  min-height: 300px;\n  padding: 30px;\n  max-width: 400px; }\n\n.signup-container .title {\n  display: block;\n  margin-bottom: 30px; }\n\n.signup-content > form {\n  display: block;\n  width: inherit; }\n\n.signup-content > form > .signup-input-fields {\n  display: block;\n  margin-bottom: 20px; }\n\n.signup-content > form > .signup-input-fields > input.inputtext {\n  margin: 20px 0px;\n  width: 350px;\n  margin: 20px 0px;\n  width: 350px; }\n\n.signup-content > form > .signup-input-fields > input.inputtext:focus {\n  border: 1px #2196F3 solid; }\n\n.signup-content > form > .signup-form-actions {\n  display: block;\n  margin-bottom: 10px; }\n\n.signup-content > form > .signup-form-actions button.button {\n  display: block;\n  width: 372px; }\n\n.signup-form-actions > span.form-message {\n  color: #888;\n  display: block;\n  font-size: 15px;\n  font-weight: 400;\n  line-height: 20px;\n  margin-top: 10px; }\n\n.signup-form-actions > span.form-message > a {\n  display: inline-block;\n  color: #888;\n  font-weight: bold;\n  text-decoration: none; }\n", ""]);

// exports


/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(391);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(40)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./signup.scss", function() {
			var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./signup.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ })

});