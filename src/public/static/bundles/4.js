webpackJsonp([4],{

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__(88);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

__webpack_require__(397);

var Messages = function (_Component) {
  _inherits(Messages, _Component);

  function Messages(props) {
    _classCallCheck(this, Messages);

    var _this = _possibleConstructorReturn(this, (Messages.__proto__ || Object.getPrototypeOf(Messages)).call(this, props));

    _this.state = {
      currentThread: {
        id: null,
        users: []
      }
    };
    return _this;
  }

  _createClass(Messages, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var root = document.querySelector("#app");
      root.className = "greenBar waterfall";
    }
  }, {
    key: '_create',
    value: function _create() {}
  }, {
    key: 'render',
    value: function render() {
      var threadid = this.props.match.params.threadid;

      return _react2.default.createElement(
        'div',
        { className: 'messages-holder' },
        _react2.default.createElement(
          'div',
          { className: 'messages-box' },
          _react2.default.createElement(
            'div',
            { className: 'left-col' },
            _react2.default.createElement(
              'div',
              { className: 'thread-header clearfix' },
              _react2.default.createElement(
                'h2',
                null,
                'Chats'
              ),
              _react2.default.createElement(
                'span',
                { className: 'create-new' },
                _react2.default.createElement(
                  _reactRouterDom.Link,
                  { to: '/messages/new', onClick: this._create.bind(this) },
                  'Create'
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'right-col' },
            _react2.default.createElement(
              'div',
              { className: 'message-header' },
              _react2.default.createElement(
                'div',
                { className: 'mh-wrap' },
                _react2.default.createElement(
                  'div',
                  { className: 'user-title' },
                  'Ahmed Ahmed'
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Messages;
}(_react.Component);

exports.default = Messages;

/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)();
// imports


// module
exports.push([module.i, ".messages-holder {\n  display: block;\n  min-height: 40px; }\n\n.messages-box {\n  background: #FFF;\n  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);\n  border-radius: 3px;\n  display: block;\n  margin: 0 auto;\n  margin-top: 125px;\n  min-height: 600px;\n  position: relative;\n  top: 0;\n  z-index: 2000;\n  width: 980px; }\n\n.left-col {\n  border-right: 1px #EEE solid;\n  display: block;\n  float: left;\n  height: 600px;\n  width: 250px; }\n\n.left-col .thread-header {\n  border-bottom: 1px #EEE solid;\n  display: block;\n  padding: 10px; }\n\n.thread-header h2 {\n  display: block;\n  float: left;\n  padding-left: 15px; }\n\n.thread-header span.create-new {\n  display: block;\n  float: right;\n  margin-right: 10px;\n  margin-top: 5px; }\n\n.thread-header span.create-new a {\n  color: #045bb8;\n  text-decoration: none; }\n\n.thread-header span.create-new a:hover {\n  text-decoration: underline; }\n\n.right-col {\n  display: block;\n  float: left;\n  height: 600px;\n  width: 729px; }\n\n.right-col .message-header {\n  display: block;\n  border-bottom: 1px #EEE solid;\n  height: 44px; }\n\n.right-col .message-header .mh-wrap {\n  padding: 14px; }\n\n.mh-wrap .user-title {\n  display: block;\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold; }\n", ""]);

// exports


/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(390);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(40)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./messages.scss", function() {
			var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./messages.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ })

});