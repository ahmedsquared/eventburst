webpackJsonp([16],{

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(5);

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__(78);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function clearCookie(name, domain, path) {
  document.cookie = encodeURIComponent(name) + "=deleted; expires=" + new Date(0).toUTCString();
};

var Feed = function (_Component) {
  _inherits(Feed, _Component);

  function Feed(props) {
    _classCallCheck(this, Feed);

    return _possibleConstructorReturn(this, (Feed.__proto__ || Object.getPrototypeOf(Feed)).call(this, props));
  }

  _createClass(Feed, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var root = document.querySelector("#app");
      root.className = "blueBar";
    }
  }, {
    key: '_handleLogout',
    value: function _handleLogout(evt) {
      var _this2 = this;

      evt.preventDefault();
      if (sessionStorage.getItem("authToken")) {
        sessionStorage.removeItem("authToken");
      }
      this.props.actions.logoutUser().then(function (response) {
        var data = response.payload.data;
        clearCookie("authToken", document.domain, "/");
        _this2.props.history.replace("/");
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'test' },
        _react2.default.createElement(
          _reactRouterDom.Link,
          { to: '/logout', onClick: this._handleLogout.bind(this) },
          'Log Out'
        )
      );
    }
  }]);

  return Feed;
}(_react.Component);

exports.default = Feed;

/***/ })

});