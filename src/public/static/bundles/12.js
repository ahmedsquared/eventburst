webpackJsonp([12],{

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

__webpack_require__(381);

var Paper = function (_Component) {
  _inherits(Paper, _Component);

  function Paper(props) {
    _classCallCheck(this, Paper);

    return _possibleConstructorReturn(this, (Paper.__proto__ || Object.getPrototypeOf(Paper)).call(this, props));
  }

  _createClass(Paper, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "div",
        { className: "paper " + this.props.style },
        _react2.default.createElement(
          "div",
          { className: "paper_wrap" },
          this.props.children
        )
      );
    }
  }]);

  return Paper;
}(_react.Component);

exports.default = Paper;

/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)();
// imports


// module
exports.push([module.i, ".paper {\n  margin: 0 auto;\n  background-color: #FFF;\n  border-radius: 2px;\n  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.15);\n  max-width: 980px; }\n\n.paper_wrap {\n  margin: 10px; }\n", ""]);

// exports


/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(379);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(40)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./paper.scss", function() {
			var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/index.js!./paper.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 384:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _reactRouter = __webpack_require__(375);

var _Paper = __webpack_require__(378);

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

__webpack_require__(396);

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var Login = function (_Component) {
  _inherits(Login, _Component);

  function Login(props) {
    _classCallCheck(this, Login);

    var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this, props));

    _this.state = {
      username: "",
      password: "",
      hasError: false,
      error: ""
    };
    return _this;
  }

  _createClass(Login, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var root = document.querySelector("#app");
      root.className = "waterfall";
    }
  }, {
    key: '_handleLogin',
    value: function _handleLogin(evt) {
      var _this2 = this;

      evt.preventDefault();
      var actions = this.props.actions;


      actions.loginUser(this.state.username.toLowerCase(), this.state.password).then(function (response) {
        var data = response.payload.data;
        // all is good.
        if (data.success == true) {
          // set sessionStorage as well.
          localStorage.setItem("EB_USERID", data.token);
        } else {
          // show error message.
          _this2.setState({ hasError: true, error: data.message });
        }
      });
    }
  }, {
    key: '_showError',
    value: function _showError() {
      return _react2.default.createElement(
        'div',
        { className: 'error-box' },
        _react2.default.createElement(
          'span',
          null,
          this.state.error
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        { className: 'bottom-area' },
        _react2.default.createElement(
          'div',
          { className: 'front-content' },
          _react2.default.createElement(
            'div',
            { className: 'auth-area' },
            _react2.default.createElement(
              _Paper2.default,
              { style: 'login_container' },
              _react2.default.createElement(
                'div',
                { className: 'title' },
                _react2.default.createElement(
                  'h1',
                  null,
                  'Log In'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'login_form' },
                this.state.hasError ? this._showError() : null,
                _react2.default.createElement(
                  'form',
                  { className: 'form', onSubmit: this._handleLogin.bind(this) },
                  _react2.default.createElement('input', { type: 'text', className: 'inputtext', name: 'username', id: 'username', onChange: function onChange(evt) {
                      return _this3.setState({ username: evt.target.value });
                    }, placeholder: 'Username or Phone Number' }),
                  _react2.default.createElement('input', { type: 'password', className: 'inputtext', name: 'password', id: 'password', onChange: function onChange(evt) {
                      return _this3.setState({ password: evt.target.value });
                    }, placeholder: 'Password' }),
                  _react2.default.createElement(
                    'div',
                    { className: 'forgot' },
                    _react2.default.createElement(
                      'a',
                      { href: '/accounts/forgot', className: 'text' },
                      'Forgot your password?'
                    )
                  ),
                  _react2.default.createElement(
                    'button',
                    { className: 'button blue' },
                    'Log In'
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Login;
}(_react.Component);

exports.default = Login;

/***/ }),

/***/ 389:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(39)();
// imports


// module
exports.push([module.i, ".login_container {\n  max-width: 400px;\n  min-height: 200px;\n  padding: 30px; }\n\n.login_container .login_form,\n.login_container .login_form > form {\n  display: block;\n  width: inherit; }\n\n.login_container .title {\n  display: block;\n  margin-bottom: 30px; }\n\n.login_container .inputtext {\n  margin: 20px 0px;\n  width: 350px; }\n\n.login_container .forgot {\n  display: block;\n  padding-bottom: 20px; }\n\n.login_container .forgot a.text {\n  color: #045bb8;\n  display: block;\n  text-decoration: none; }\n\n.login_container .forgot a.text:hover {\n  color: #01579b; }\n\n.login_container .button {\n  width: 370px; }\n", ""]);

// exports


/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(389);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(40)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./login.scss", function() {
			var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/sass-loader/index.js!./login.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ })

});