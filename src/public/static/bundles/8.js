webpackJsonp([8],{

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = __webpack_require__(88);

var _asyncComponent = __webpack_require__(90);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Profile = function (_Component) {
  _inherits(Profile, _Component);

  function Profile(props) {
    _classCallCheck(this, Profile);

    return _possibleConstructorReturn(this, (Profile.__proto__ || Object.getPrototypeOf(Profile)).call(this, props));
  }

  // load user data.


  _createClass(Profile, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this._getUsernameData();
    }
  }, {
    key: '_getUsernameData',
    value: function _getUsernameData() {
      // auto set state.
      this.props.actions.getProfileUser(this.props.match.params.username, sessionStorage.getItem("authToken"));
    }
  }, {
    key: '_renderProfile',
    value: function _renderProfile() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h1',
          null,
          this.props.user.firstName + " " + this.props.user.lastName
        )
      );
    }
  }, {
    key: '_renderPage',
    value: function _renderPage() {
      var username = this.props.match.params.username;

      if (this.props.viewer.id == undefined) {
        if (username == "signup" || username == "login") {
          var Home = (0, _asyncComponent.asyncComponent)(function () {
            return __webpack_require__.e/* import() */(0/* duplicate */).then(__webpack_require__.bind(null, 367)).then(function (module) {
              return module.default;
            });
          });
          return _react2.default.createElement(Home, this.props);
        }
      } else {
        if (username == "signup" || username == "login") {
          this.props.history.replace("/");
        }
        return this._renderProfile();
      }
      return this._renderProfile();
    }
  }, {
    key: 'render',
    value: function render() {
      var username = this.props.match.params.username;

      return _react2.default.createElement(
        'div',
        null,
        this._renderPage()
      );
    }
  }]);

  return Profile;
}(_react.Component);

exports.default = Profile;

/***/ })

});