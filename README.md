# Eventburst

Eventburst is a location based service that allows you to discover things happening around first. Provides you with tools to create events and getting guests over to the event. Proper Planning tools also available.

## Goal

The reason for this project is to solve the problem of figuring out about projects too late.

## Installation

Pull the repo. Install Webpack, NodeJS, and Postgres. Run webpack --colors --progress --watch in the folder src/build. Run Node on node src/server.js.

## Contributors

Submit a pull request to have your changes applied to master. Submit them in the beta branch.

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

No License.git
